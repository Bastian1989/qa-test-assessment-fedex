import SearchFormPO from '../page-objects/search-form.po';

const { When, Then } = require('cucumber');
const chai = require('chai');
chai.use(require('chai-as-promised'));
const expect = chai.expect;


When('the user searches for planet {string}', async name => {
  await SearchFormPO.searchPlanets(name);
});

Then('the user sees the details of planet {string}', async name => {
  const itemNames = await SearchFormPO.getPlanetNames();
  expect(itemNames).to.contain(name);
});

Then('the planet cannot be found', async () => {
  await expect(SearchFormPO.notFoundElement.isPresent()).to.eventually.be.true;
});
