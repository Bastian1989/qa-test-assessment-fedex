import SearchFormPO from '../page-objects/search-form.po';

const { When, Then } = require('cucumber');
const chai = require('chai');
chai.use(require('chai-as-promised'));
const expect = chai.expect;


When('the user searches for person {string}', async name => {
  await SearchFormPO.searchPeople(name);
});

Then('the user sees the details of person {string}', async name => {
  const itemNames = await SearchFormPO.getPeopleNames();
  expect(itemNames).to.contain(name);
});

Then('person cannot be found', async () => {
  await expect(SearchFormPO.notFoundElement.isPresent()).to.eventually.be.true;
});
