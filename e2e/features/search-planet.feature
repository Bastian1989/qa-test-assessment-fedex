Feature: Search for a Star Wars planet

    Scenario: By planet name
        Given the user opens the home page
        When the user searches for planet "Tatooine"
        Then the user sees the details of planet "Tatooine"

    Scenario: Not a valid character
        Given the user opens the home page
        When the user searches for planet "Mars"
        Then the planet cannot be found
