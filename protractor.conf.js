// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

var fs = require("fs");
var reporter = require("cucumber-html-reporter");

var htmlReportOptions = {
  theme: 'bootstrap',
  jsonFile: 'test-reports/cucumber-test-results.json',
  output: 'test-reports/cucumber_report.html',
  reportSuiteAsScenarios: true,
  scenarioTimestamp: true,
  launchReport: true,
};

exports.config = {
  debug: true,
  specs: ["./e2e/features/**/*.feature"],
  capabilities: {
    browserName: "chrome",
  },
  directConnect: true,
  baseUrl: "http://localhost:4200/",
  framework: "custom",
  frameworkPath: require.resolve("protractor-cucumber-framework"),
  cucumberOpts: {
    timeout: 1,
    strict: true,
    require: ["./e2e/**/*.steps.ts"],
    format: ["json:test-reports/cucumber-test-results.json"],
  },
  onPrepare() {
    fs.mkdirSync(__dirname + "/test-reports", { recursive: true });

    require("ts-node").register({
      project: require("path").join(__dirname, "./e2e/tsconfig.e2e.json"),
    });
  },

  onCleanUp() {
    reporter.generate(htmlReportOptions);
  }
};
